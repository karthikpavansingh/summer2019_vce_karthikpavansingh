from django.db import models
import datetime
# Create your models here.
class todolist(models.Model):
    name=models.CharField(max_length=128)
    created=models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return self.name

class todoitem(models.Model):
    description=models.CharField(max_length=128)
    due_date=models.DateField(null=True)
    completed=models.BooleanField(default=False)
    listid= models.ForeignKey(todolist, on_delete=models.CASCADE)

