from onlineapp.models import *
from django.forms import *

class addcollege(ModelForm):
    class Meta:
        model=college
        exclude=['id']
        widgets={
            'name':TextInput(attrs={'placeholder':'Enter name'}),
            'location':TextInput(attrs={'placeholder':'Enter location'}),
            'acronym':TextInput(attrs={'placeholder':'Enter Acronym'}),
            'contact':EmailInput(attrs={'placeholder':'Enter Contact'}),
        }

class addstudent(ModelForm):
    class Meta:
        model=student
        exclude=['id','dob','college']
        widgets={
            'name': TextInput(attrs={'placeholder': 'Enter name'}),
            'email': EmailInput(attrs={'placeholder': 'Enter email'}),
            'db_folder': TextInput(attrs={'placeholder': 'Enter db_folder name'}),
            'dropped_out': CheckboxInput(attrs={'placeholder': 'Enter db_folder name'}),
        }

class addmarks(ModelForm):
    class Meta:
        model=MockTest1
        exclude=['total','student']
        widgets={
            'Problem1': NumberInput(attrs={'placeholder': 'Enter score'}),
            'Problem2': NumberInput(attrs={'placeholder': 'Enter score'}),
            'Problem3': NumberInput(attrs={'placeholder': 'Enter score'}),
            'Problem4': NumberInput(attrs={'placeholder': 'Enter score'}),
        }
