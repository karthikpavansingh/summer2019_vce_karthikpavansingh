from onlineapp.models import *
from django.forms import *


class loginform(Form):
    username=CharField(max_length=128,label='user-name',widget=TextInput(attrs={'placeholder':'enter username'}))
    password=CharField(max_length=128,label='password',widget=PasswordInput(attrs={'placeholder':'enter password'}))


class signupform(Form):
    first_name=CharField(max_length = 128, label = 'first-name', widget = TextInput(attrs={'placeholder': 'enter first name'}))
    last_name=CharField(max_length = 128, label = 'last-name', widget = TextInput(attrs={'placeholder': 'enter last name'}))
    username=CharField(max_length = 128, label = 'user-name', widget = TextInput(attrs={'placeholder': 'enter username'}))
    password=CharField(max_length = 128, label = 'password', widget = PasswordInput(attrs={'placeholder': 'enter password'}))

