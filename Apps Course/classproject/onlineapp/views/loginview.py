from django.contrib import messages
from django.http import HttpResponse
from django.template import loader
from django.urls import resolve
from django.views import View
from onlineapp.models import *
from django.shortcuts import render,redirect
from onlineapp.forms import *
from django.contrib.auth import authenticate, logout,login
from django.contrib.auth.models import *


def logout_user(request):
    logout(request)
    return redirect('login_page')


class loginController(View):
    def get(self,request,*args,**kwargs):
        if request.user.is_authenticated:
             return redirect('college_html')
        login=loginform()
        return render(request,template_name='onlineapp/login.html',context={'form':login})
    def post(self,request,*args,**kwargs):
        username=request.POST['username']
        password=request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request,user)
            return redirect('college_html')
        else:
            messages.error(request,'Invalid credentials...Give it a another try?')
            return redirect('login_page')


class SignupController(View):
    def get(selfself,request,*args,**kwargs):
        signup=signupform()
        return render(request,template_name='onlineapp/signup.html',context={'form':signup})
    def post(self,request,*args,**kwargs):
        form=signupform(request.POST)
        user=User.objects.create_user(**form.cleaned_data)
        user.save()
        if user is not None:
            return redirect('login_page')
        else:
            messages.error(request, 'Details that you gave seems to be  ambiguous ')
            return redirect('signup_page')
