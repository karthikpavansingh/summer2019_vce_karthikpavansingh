from django.http import HttpResponse
from django.template import loader
from django.urls import resolve
from django.views import View
from onlineapp.models import *
from django.shortcuts import render,redirect
from onlineapp.forms.students import *
from django.contrib.auth.mixins import LoginRequiredMixin


class CollegeView(LoginRequiredMixin,View):
    login_url='/login/'
    def get(self,request,*args,**kwargs):
        user_permissions=request.user.get_all_permissions()
        if kwargs:
            c = college.objects.get(**kwargs)
            students=list(c.student_set.order_by('-mocktest1__total'))
            return render(request, template_name="onlineapp/studentdetails.html", context={'students': students,'Title':c.name,'id':c.id,'permissions':user_permissions})

        colleges=college.objects.all()
        return render(
            request,
            template_name="onlineapp/colleges.html",
            context={
                'key':colleges,
                'Title':'All colleges| Mentor APP',
                'permissions': user_permissions
            }
        )

class ADDcollege(LoginRequiredMixin,View):
    login_url = '/login/'
    def get(self,request,*args,**kargs):
        data=addcollege()
        if resolve(request.path_info).url_name == "deleteinfo":
            college.objects.get(id=kargs['id']).delete()
            return redirect('college_html')

        if kargs:
            col=college.objects.get(id=kargs['id'])
            data=addcollege(instance=col)
        return render(request,template_name='onlineapp/register_college.html',context={'form':data,'title':'register'})

    def post(self,request,*args,**kargs):
        if resolve(request.path_info).url_name=='editinfo':
            col=college.objects.get(id=kargs['id'])
            data = addcollege(request.POST,instance=col)
            if (data.is_valid()):
                data.save()
                return redirect('college_html')
        data=addcollege(request.POST)
        if(data.is_valid()):
            data.save()
            return redirect('college_html')


class ADDstudent(LoginRequiredMixin,View):
    login_url = '/login/'
    def get(self,request,*args,**kwargs):
        form1=addstudent()
        form2=addmarks()
        if resolve(request.path_info).url_name == "deletestudent":
            s=student.objects.get(**kwargs)
            s.delete()
            kwargs['id'] = s.college.id
            return redirect('students_html',**kwargs)

        if kwargs and resolve(request.path_info).url_name=='editstudent':
            stud=student.objects.get(**kwargs)
            form1=addstudent(instance=stud)
            num_results = MockTest1.objects.filter(student=stud).count()
            if num_results:
                mock=MockTest1.objects.get(student=stud)
                form2=addmarks(instance=mock)
        return render(request,template_name="onlineapp/register_student.html",context={'form1':form1,'form2':form2})

    def post(self,request,*args,**kwargs):
        if resolve(request.path_info).url_name == 'editstudent':
            stud=student.objects.get(**kwargs)

        else:
            col=college.objects.get(id=kwargs.get('id'))
            stud=student(college=col)

        form1 = addstudent(request.POST,instance=stud)
        if form1.is_valid():
            form1.save()
            tot=sum([int(request.POST['problem'+str(i)]) for i in range(1,5)])
            if resolve(request.path_info).url_name == 'editstudent':
                marks = MockTest1.objects.get(student=stud)
            else:
                marks = MockTest1(student=stud)
            if(marks):
                marks.total=tot
                form2=addmarks(request.POST,instance=marks)
                if form2.is_valid():
                    form2.save()
        kwargs['id']=stud.college.id
        return redirect('students_html',**kwargs)
