from django.contrib import admin

# Register your models here.
from .models import college
from .models import MockTest1
from .models import Teacher
from .models import student

admin.site.register(college)
admin.site.register(student)
admin.site.register(MockTest1)
admin.site.register(Teacher)