from django.urls import path, include
from onlineapp.views import *

urlpatterns=[
    # path('getcollege/',get_College),
    path('',lambda x: redirect('college_html')),
    path('colleges/<int:id>',CollegeView.as_view(),name='students_html'),

    path('colleges/',CollegeView.as_view(),name='college_html'),
    path('college/add',ADDcollege.as_view(),name='register_html'),
    path('colleges/<int:id>/edit',ADDcollege.as_view(),name='editinfo'),
    path('colleges/<int:id>/delete', ADDcollege.as_view(), name='deleteinfo'),
    path('college/<int:id>/addstu',ADDstudent.as_view(),name='addstudent'),
    path('college/<int:id>/edit',ADDstudent.as_view(),name='editstudent'),
    path('college/<int:id>/delete',ADDstudent.as_view(),name='deletestudent'),
    path('colleges/<str:acronym>', CollegeView.as_view(), name='students2_html'),

    path('login/',loginController.as_view(),name='login_page'),
    path('signup/',SignupController.as_view(),name='signup_page'),
    path('logout/',logout_user,name='logout_page')

]