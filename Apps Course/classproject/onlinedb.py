import click
import os
import django
from openpyxl import load_workbook, Workbook
from bs4 import BeautifulSoup
os.environ.setdefault('DJANGO_SETTINGS_MODULE','classproject.settings')
django.setup()
from onlineapp.models import *

@click.group()
def cli():
    pass

@cli.command()
@click.argument('excel_file')
def importcollegedata(excel_file):
    wb=load_workbook(excel_file)
    sheets=wb.sheetnames
    for sheet in sheets:
        if sheet=='Colleges':
            for row in wb[sheet].values:
                if row[0]!='College Name':
                    c=college(name=row[0],location=row[2],acronym=row[1],contact=row[3])
                    c.save()

@cli.command()
@click.argument('excel_file')
def importstudentdata(excel_file):
    wb = load_workbook(excel_file)
    sheets = wb.sheetnames
    for sheet in sheets:
        if sheet == 'Current':
            for row in wb[sheet].values:
                if row[0]!='Name':
                    s=student(name=row[0],college=college.objects.get(acronym=row[1]),email=row[2],db_folder=row[3])
                    s.save()
        elif sheet =='Deletions':
            for row in wb[sheet].values:
                if row[0]!='Name':
                    s=student(name=row[0],college=college.objects.get(acronym=row[1]),email=row[2],db_folder=row[3],dropped_out=True)
                    s.save()

@cli.command()
@click.argument('excel_file')
def importmarks(excel_file):
    wb = load_workbook(excel_file)
    sheets = wb.sheetnames
    for sheet in sheets:
        for row in wb[sheet].values:
            if row[0]!='Student':
                student_dbname=row[0].split('_')[2]
                m=MockTest1(problem1=row[1],problem2=row[2],problem3=row[3],problem4=row[4],total=row[5],student=student.objects.get(db_folder=student_dbname))
                m.save()

cli()