#include<stdio.h>
#include<stdlib.h>
long pow(int a, int n,long m)
{
	if (n == 0) return 1;
	if (n == 1) return a;
	int t = pow(a, n / 2,m);
	return n&1 == 1 ? (t%m*t%m*a%m)%m : (t%m*t%m)%m;
}
/*
int main()
{
	printf("%d", pow(18, 2,1000000007));
	getchar();
	return 0;
}
*/