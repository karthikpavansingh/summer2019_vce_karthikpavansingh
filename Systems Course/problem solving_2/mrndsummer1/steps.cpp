#include<stdio.h>
int getSteps(int s,int k) {
	if (s == 1 )
		return 1;
	if (s <=k)
		return s;

	int sum = 0;
	for (int i = 1; i <= k; i++)
		sum += getSteps(s - i,k);
	return sum;
}
/*
int main()
{
	printf("%d", getSteps(3, 3));
	getchar();
	return 0;
}
*/