#include<stdio.h> 
#include<stdlib.h> 

struct Node
{
	int data;
	struct Node* next;
};


int detectLoop(struct Node *list)
{
	struct Node  *slow  = list, *fast  = list;

	while (slow  && fast  && fast ->next)
	{
		slow  = slow ->next;
		fast  = fast ->next->next;

		
		if (slow  == fast )
		{
			return 1;
		}
	}

	return 0;
}


struct Node *newNode(int key)
{
	struct Node *temp = (struct Node*)malloc(sizeof(struct Node));
	temp->data = key;
	temp->next = NULL;
	return temp;
}

/*
int main()
{
	struct Node *head = newNode(50);
	head->next = newNode(20);
	head->next->next = newNode(15);
	head->next->next->next = newNode(4);
	head->next->next->next->next = newNode(10);

	head->next->next->next->next->next = head->next->next;

	int t=detectLoop(head);
	if (t)
	printf("detected loop \n");
	else printf("no loop detected\n");
	getchar();
	return 0;
}
*/