/*
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
int compare(char s[], char a[][3], int rs, int cs, int re, int ce,int direction)
{

	int index = 0;
	if (direction == 0)
	{
		for (int i = cs; i >=ce; i--)
		{
			if (s[index++] == a[i][cs])
				continue;
			else return 0;
		}
		return 1;
	}
	if (direction == 1)
	{
		int j = cs;
		for (int i = rs; i >=re; i--)
		{
			r[0] = a[i][j--];
			r[1] = '\0';
			 
		}
	}
	if (direction == 2)
	{
		for (int i = rs; i >=re; i--)
		{
			r[0] = a[i][ce];
			r[1] = '\0';
			 
		}
	}
	if (direction == 3)
	{
		int j = rs;
		for (int i = cs; i < ce; i++)
		{
			r[0] = a[i--][j];
			r[1] = '\0';
			 
		}
	}
	if (direction == 4)
	{
		for (int i = cs; i <ce; i++)
		{
			r[0] = a[rs][i];
			r[1] = '\0';
			 
		}
	}
	if (direction == 5)
	{
		int j = rs;
		for (int i = cs; i < ce; i++)
		{
			r[0] = a[j++][i];
			r[1] = '\0';
			 
		}
	}
	if (direction == 6)
	{
		for (int i = rs; i < re; i++)
		{
			r[0] = a[i][ce];
			r[1] = '\0';
			 
		}
	}
	if (direction == 7)
	{
		int j = rs;
		for (int i = cs; i > ce; i--)
		{
			r[0] = a[j++][ce];
			r[1] = '\0';
			 
		}
	}
	if (strcmp(f, s) == 0)return 1;
	else return 0;
}
int count_occ(char a[][3], int m,int n, char s[],int count,int direction,int rs,int cs,int re,int ce)
{
	
	if (re < 0 || ce < 0 || re >= m || ce >= n)
		return 0;
	if (direction == 0)
	{
			if (compare(s, a, rs, cs, re, ce,direction))
				count++;
			else{
				ce--;
				count_occ(a, m, n, s, count, 0, rs, cs, re,ce);
				
				}
		
		count_occ(a, m, n, s, count, 1, rs, cs, rs - 1, cs - 1);
	}
	
	if (direction == 1)
	{
			if (compare(s, a, rs, cs, re, ce, direction))
				count++;
			else{
				ce--;
				re--;
				count_occ(a, m, n, s, count, 1, rs, cs, re, ce);
			
			}
		

		count_occ(a, m, n, s, count, 2, rs, cs, rs-1 , cs);
	}
	if (direction == 2)
	{
			if (compare(s, a, rs, cs, re, ce, direction))
				count++;
			else{
				re--;
				count_occ(a, m, n, s, count, 2, rs, cs, re, ce);
				}
		

		count_occ(a, m, n, s, count, 3, rs, cs, rs -1, cs+1);
	}
	if (direction == 3)
	{
		
			if (compare(s, a, rs, cs, re, ce, direction))
				count++;
			else{
				ce++;
				re--;
				count_occ(a, m, n, s, count, 3, rs, cs, re, ce);
				
			}

		count_occ(a, m, n, s, count, 4, rs, cs, rs , cs + 1);
	}
	if (direction == 4)
	{
		
			if (compare(s, a, rs, cs, re, ce, direction))
				count++;
			else{
				ce++;
				count_occ(a, m, n, s, count, 4, rs, cs, re, ce);
				
			}

		count_occ(a, m, n, s, count, 5, rs, cs, rs +1, cs+1 );
	}
	if (direction == 5)
	{
			if (compare(s, a, rs, cs, re, ce, direction))
				count++;
			else{
				re++;
				ce++;
				count_occ(a, m, n, s, count, 5, rs, cs, re, ce);
				
			}
		

		count_occ(a, m, n, s, count, 6, rs, cs, rs + 1, cs );
	}
	if (direction == 6)
	{
		
			if (compare(s, a, rs, cs, re, ce, direction))
				count++;
			else{
				re++;
				count_occ(a, m, n, s, count, 6, rs, cs, re, ce);
		
			}
		

		count_occ(a, m, n, s, count, 7, rs, cs, rs + 1, cs - 1);
	}
	if (direction == 7)
	{
		
			if (compare(s, a, rs, cs, re, ce, direction))
				count++;
			else{
				re++;
				ce--;
				count_occ(a, m, n, s, count, 7, rs, cs, re, ce);
			
			
		}

	}
	
	return count;
}
/*
int main()
{
	char a[][3] = {
		{ 'h', 'a', 'z' },
		{ 'a', 'b', 'c' },
		{ 'z', 'f', 'r' }
	};
	char c[] = "ha";
	int count = 0;
//	count=count_occ(a, 3, 3, c, count, 0, 0, 0, 0, -1);
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
		{
			if (c[0] == a[i][j])
				count += count_occ(a, 3, 3, c, 0, 0, i, j, i, j - 1);
		}
	count = compare(c, a, 0, 1, 0, 0, 0);
	getchar();
	return 0;
}
*/
#include<map>

using namespace std;

struct node{
	int data;
	struct node* next, *random;
};
typedef struct node Node;

struct node* create_node(int data)
{
	Node* temp = (Node*)malloc(sizeof(*temp));
	temp->data = data;
	temp->next = NULL;
	temp->random = NULL;
	return temp;
}

struct node* clone_nh(struct node* head)
{
	struct node* clone_head = NULL, *temp = head, *clone_temp = NULL;
	map <Node*, Node*> hash;

	while (temp)
	{
		if (!clone_temp)
		{
			clone_head = clone_temp = create_node(temp->data);

		}
		else
		{
			clone_temp->next = create_node(temp->data);
			clone_temp = clone_temp->next;
		}
		hash.insert(pair<Node*, Node*>(temp, clone_temp));
		temp = temp->next;
	}


	temp = head;
	clone_temp = clone_head;
	while (temp)
	{
		clone_temp->random = hash[temp->random];
		temp = temp->next;
		clone_temp = clone_temp->next;
	}
	return clone_head;
}

int main()
{
	Node* start = create_node(1);
	start->next = create_node(2);
	start->next->next = create_node(3);
	start->next->next->next = create_node(4);
	start->next->next->next->next = create_node(5);

	start->random = start->next->next;
	start->next->random = start;
	start->next->next->random = start->next->next->next->next;
	start->next->next->next->random = start->next->next->next->next;
	start->next->next->next->next->random = start->next;

	Node* clone_head = clone_nh(start);
	while (start)
	{
		if (start->data != clone_head->data)
		{
			printf("not same");
			getchar();
			return 1;
		}
		if (start->random->data != clone_head->random->data)
		{
			printf("not same random ptr");
			getchar();
			return 1;
		}
		start = start->next;
		clone_head = clone_head->next;
	}
	printf("Cloned succesfully\nEnter any key to continue...");
	getchar();
	return 0;
}