// MRNDsummer.cpp : Defines the entry point for the console application.


#include<stdio.h>
#include<map>
#include<stdlib.h>
struct node
{
	int data;
	struct node*next;
	struct node*rand;
};

struct node* createnode(int d)
{
	struct node* created = (struct node*)malloc(sizeof(struct node));
	created->data = d;
	created->next = NULL;
	created->rand = NULL;
	return created;
}
struct node* clone_with_hashmap(struct node*head)
{
	struct node*temp = head;
	struct node *newhead=NULL;
	struct node *temp2=NULL;
	std::map <struct node*, struct node*> m;
	while (temp)
	{
		if (temp == head)
		{
			newhead = createnode(temp->data);
			temp2 = newhead;
			m[temp] = newhead;
			temp = temp->next;
		}
		else{
			temp2->next = createnode(temp->data);
			temp2 = temp2->next;
			m[temp] = temp2;
			temp = temp->next;

		}
	}
		
		temp = head;
		temp2 = newhead;
		while (temp)
		{
			m[temp]->rand = temp->rand;
			temp = temp->next;
		}
	

	
	return newhead;

}

struct node* clone_optimal(struct node*head)
{
	struct node*temp = head;
	struct node*newhead = NULL;
	struct node*nextnode=NULL;
	struct node*temp2 = NULL;
	while (temp)
	{
		nextnode = temp->next;
		temp->next = createnode(temp->data);
		temp = temp->next;
		temp->next = nextnode;
		if(temp)temp = temp->next;
	}
	temp = head;
	while (temp){
		temp->next->rand = temp->rand?temp->rand->next:temp->rand;

		temp = temp->next->next;
	}
	temp = head;
	newhead = head->next;
	temp2 = newhead;
	while (temp&&temp2)
	{
		temp->next = temp->next?temp->next->next:temp->next;
			temp2->next = temp2->next?temp2->next->next:temp2->next;
		temp2 = temp2->next;
		temp = temp->next;
	}
	
	return newhead;
}/*
int main()
{
	
	struct node* n1 = (struct node*)malloc(sizeof(struct node));
	struct node* n2 = (struct node*)malloc(sizeof(struct node));
	struct node* n3 = (struct node*)malloc(sizeof(struct node));
	struct node* n4 = (struct node*)malloc(sizeof(struct node));
	struct node* n5 = (struct node*)malloc(sizeof(struct node));
	struct node* n6 = (struct node*)malloc(sizeof(struct node));

	struct node*head = n1;
	n1->data = 1;
	n2->data = 2;
	n3->data = 3;
	n4->data = 4;
	n5->data = 5;
	n6->data = 6;

	n1->next = n2;
	n2->next = n3;
	n3->next = n4;
	n4->next = n5;
	n5->next = n6;
	n6->next = NULL;

	n1->rand = n3;
	n2->rand = n5;
	n3->rand = n2;
	n4->rand = n5;
	n5->rand = n6;
	n6->rand = NULL;
	struct node*t = clone_with_hashmap(head);
	while (t)
	{
		printf("%d ", t->data);
		t = t->next;
	}
	//print this ..and verify with i/p for both next and random
	
	getchar();


	return 0;
}

*/