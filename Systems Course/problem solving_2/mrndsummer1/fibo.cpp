#include<stdio.h>
#include<stdlib.h>
#include<map>
int fibo(int n,std::map <int,int> &m )
{
	if (n < 0)return 0;
	if (n == 0 || n == 1){
		m[n] = n;
		return m[n];
	}
	else{
		if (m[n])
		{
			m[n - 1] = fibo(n - 1,m);
			m[n - 2] = fibo(n - 2,m);
		}
		m[n]=m[n - 1] + m[n - 2];
	}
	return m[n];
}
/*
int main()
{
	std::map<int, int> m;
	printf("%d",fibo(7,m));
	getchar();
}
*/