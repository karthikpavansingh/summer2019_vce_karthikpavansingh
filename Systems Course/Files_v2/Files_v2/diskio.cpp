#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "diskio.h"
#define totalblocks 6400
static int BLOCK_SIZE=16384;
static char HDDfilename[30];
#pragma warning(disable:4996)

int init(int size, char dbfilename[])
{
	strcpy(HDDfilename, dbfilename);
	char *buff = (char*)calloc(1,16 * 1024);
	struct meta*m = (struct meta*)calloc(1,sizeof(struct meta));
	m->no_of_Tblocks = totalblocks;
	m->no_of_free_blocks = totalblocks - 1;
	m->blockavailable[0] = true;
	m->magic = 121212;
	m->nfiles = 1;
	for (int i = 0; i < 32; i++)m->file[i].nextblock = 0;
	memcpy(buff, m, sizeof(struct meta));
	writeblock(0, buff);
	return totalblocks;
}
void readblock(int block, void *buff)
{
	FILE *fdb = fopen("db.hdd", "rb+");
	fseek(fdb, block * BLOCK_SIZE, SEEK_SET);
	fread(buff, 1, BLOCK_SIZE, fdb);
	fclose(fdb);
	return;

}
void writeblock(int block, void *buff)
{
	FILE *fdb = fopen("db.hdd", "rb+");
	fseek(fdb, block * BLOCK_SIZE, SEEK_SET);
	fwrite(buff, 1, BLOCK_SIZE, fdb);
	fclose(fdb);
	return;
}