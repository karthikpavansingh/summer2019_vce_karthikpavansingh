#pragma once
#define DISK_IO

struct file{
	char filename[20];
	int start_length;
	int nblocks;
	int nextblock;
	int file_length;
};
struct meta{
	int magic;
	struct file file[32];
	int nfiles;
	int no_of_Tblocks;
	int no_of_free_blocks;
	int blockavailable[640];
};

int init(int size, char f[]);
void readblock(int block, void *bubff);
void writeblock(int block, void *buff);
