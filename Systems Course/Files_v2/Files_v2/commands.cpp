#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include "command.h"
#include "diskio.h"
#pragma warning(disable:4996)

void setblock(char source[], char dest[])
{
	struct meta *m = (struct meta*)calloc(1, sizeof(struct meta));
	FILE *write = fopen(source, "rb+");
	fseek(write, 0, SEEK_END);
	int size = ftell(write);
	int no_of_blocks = (size / 16384);
	if (size % 16384)no_of_blocks++;
	int count = 0;
	int i = 0;
	char *buff1 = (char*)calloc(1, 16 * 1024);
	readblock(0, buff1);
	memcpy(m, buff1, sizeof(struct meta));
	fseek(write, 0, SEEK_SET);
	int sum = 0;
	for ( i = 1; i < 6400; i++)
	{
		if (m->blockavailable[i]== false)
			sum++;
		if (sum == no_of_blocks+1)
			break;
	}
	if (sum-1 != no_of_blocks)printf("NO available space\n");
	else
	{
		char *buff = (char*)calloc(1,16384);
		struct file*f1 = (struct file*)calloc(1, sizeof(struct file));
		strcpy(f1->filename, dest);
		f1->file_length = size;
		f1->nblocks = no_of_blocks;
		m->file[m->nfiles] = *f1;
		int *temp = (int*)calloc(4096,4);
		int indi = 0;

		if (no_of_blocks > 1 && m->no_of_free_blocks>no_of_blocks)
		{
			int first = 1;
			for (int j = 1; j < i; j++)
			{
				if (m->blockavailable[j] == false && first == 1){
					m->file[m->nfiles].nextblock = j;

					m->file[j].start_length = j;
					first = 0;
					m->blockavailable[j] = true;
					m->no_of_free_blocks--;
					
				}
				else if (m->blockavailable[j] == false){
					m->blockavailable [j]= true;
					m->no_of_free_blocks--;
					m->file[j].nextblock = 0;

					temp[indi++] = j;
					
					fread(buff, 1, 16384, write);
					writeblock(j, buff);
				}
			}
			if (size % 16834 == 0)
				fread(buff, 1, 16384, write);
			else fread(buff, 1, size % 16384, write);
			temp[indi++] = i;
			writeblock(i, buff);
		//	char*te = (char*)malloc(16384);
		//	memcpy(te, temp, sizeof(temp));

			writeblock(m->file[m->nfiles++].nextblock,temp);
			for (int lk = 0; lk < 4; lk++)printf("%d \n", temp[lk]);
			writeblock(0, m);
		}
		else if(m->no_of_free_blocks>no_of_blocks){
			f1->start_length = i - sum + 1;
			m->blockavailable[f1->start_length] = true;
			writeblock(0, m);
			char *buffer = (char*)calloc(1, f1->nblocks * 16384 + 1);
			for (int blocks = f1->start_length; blocks < f1->start_length + f1->nblocks; blocks++)
			{
				if (size % 16834 == 0)
					fread(buffer, 1, 16384, write);
				else fread(buffer, 1, size % 16384, write);
				writeblock(blocks, buffer);
			}
		}
	}
	fclose(write);
}
int getblock(char s[],char d[])
{
	struct meta *m = (struct meta*)calloc(1, sizeof(struct meta));
	FILE *fout = fopen(d, "wb+");
	char *buff1 = (char*)calloc(1, 16 * 1024);
	readblock(0, buff1);
	memcpy(m, buff1, sizeof(struct meta));
	int i = 0;
	for ( i = 0; i < m->nfiles; i++)
	{
		if (strcmp(s, m->file[i].filename) == 0)
			break;
	}
	if (i == m->nfiles)printf("NO SUCH FILE EXIST IN SYSTEM\n");
	else{
		char*buff = (char*)calloc(1, 16384);
		if (m->file[i].nextblock){

			int *temp = (int*)calloc(4096,4);
			readblock(m->file[i].nextblock, temp);
			int r;
			for (r = 0; r < m->file[i].nblocks-1; r++)
			{
				readblock(temp[r], buff);
				fwrite(buff, 1, 16384, fout);
			}
			readblock(temp[r], buff);
			if (m->file[i].file_length%16384)
				fwrite(buff, 1, m->file[i].file_length%16384, fout);
			else
				fwrite(buff, 1, 16384, fout);
		}
		else{
			for (int j = m->file[i].start_length; j < m->file[i].start_length + m->file[i].nblocks; i++)
			{
				readblock(j, buff);
				fwrite(buff, 1, m->file[i].file_length, fout);
			}
		}
	}
	fclose(fout);
	return 0;
}
int command()
{
	char operation[50];
	char filename[20];
	int blocksize;
	int bsize;
	scanf("%s", operation);
	char presentfile[20];
	while (1)
	{
		if (strcmp(operation, "MOUNT") == 0)
		{
			
			scanf("%s%d", filename,&blocksize);
			strcpy(presentfile, filename);
			init(blocksize, filename);
			break;

		}
		if (strcmp(operation, "COPYTOFS") == 0)
		{
			printf("ok");
			char sfilename[20];
			char dfilename[20];
			scanf("%s%s", sfilename,dfilename);
			setblock(sfilename, dfilename);
			break;

		}
		if (strcmp(operation, "COPYFROMFS") == 0)
		{
			char sfilename[20];
			char dfilename[20];
			scanf("%s%s", sfilename, dfilename);
			getblock(sfilename, dfilename);
			break;
		}
		if (strcmp(operation, "FORMAT")==0)
		{
			
			scanf("%d", &bsize);
			init(bsize, presentfile);
			break;
		}
		if (strcmp(operation, "exit")==0)
			break;
		
	}
	getchar();
	return 0;
}