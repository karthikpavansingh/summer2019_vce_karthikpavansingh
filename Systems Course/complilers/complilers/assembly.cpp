#include<stdio.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
struct reg
{
	int name;
	int data;
};
struct symboltable{
	char identifier[30];
	int size;
	int addr;
};
struct intermediate
{
	int intstruction_no;
	int opcode;
	int op1;
	int op2;
	int op3;
	int jump;
};

int getsizeofsymboltable(char filename[])
{
	int size = 0;
	char buff[100];
	FILE *source = fopen(filename, "r");
	while (fgets(buff, 100, source) != NULL)
		if (buff[0] == 'D')size++;
	fclose(source);
	return size;
}
int getsizeofintermediatetable(char filename[])
{
	int size = 0;
	char buff[100];
	int flag = 0;
	FILE *source = fopen(filename, "r");
	while (fgets(buff, 100, source) != NULL)
		if (buff[0] == 'S' || flag)break;
	while (fgets(buff, 100, source) != NULL)size++;
	return size;
}
int getTokens(char line[],char **ret,char *delimiter)
{
	char *token = strtok(line, delimiter);
	int ind = 0;
	while (token != NULL) {
		strcat(ret[ind++], token);
		token = strtok(NULL, " ");
	}
	return ind;
}
int check_if_array_type(char *token,int *siz)
{
	int number = 0; int i,j;
	for (i = 0; i < strlen(token); i++)
		if (token[i] == '[')
			break;
	if (i != strlen(token)){
		*siz = i ;
		for (j = i + 1; token[j] != ']'; j++)
			number = number * 10 + token[j] - '0';
		return number;
	}
	else return 0;
	
}
void getstring(char *i, char *token,int size){
	int c =0;
	while (c < size) {
		i[c] = token[c] ;
		c++;
	}
	i[c] = '\0';
}
void buildsymboltable(char **tokens, struct symboltable **st, int symboltableindex)
{
	char *identifier = (char*)malloc(sizeof(tokens[1]));
	int sizeofidentifier = 0;
	int s = check_if_array_type(tokens[1],&sizeofidentifier);
	
	if (s){
		getstring(identifier, tokens[1], sizeofidentifier);
		strcpy(st[symboltableindex]->identifier, identifier);
	}
	else strcpy(st[symboltableindex]->identifier, tokens[1]);
	st[symboltableindex]->size = s == 0 ? 1 : s;
	if (symboltableindex == 0)st[symboltableindex]->addr = 0;
	else st[symboltableindex]->addr = st[symboltableindex - 1]->size + st[symboltableindex - 1]->addr;
}
int getaddrs(struct symboltable **st, char *a,int size){
	for (int i = 0; i < size; i++)
		if (strcmp(st[i]->identifier, a) == 0)
			return st[i]->addr;
}
void processtoken(char **tokens,struct symboltable**st,int sizeofst,struct intermediate **it,int index)
{
	if (strcmp(tokens[0], "MOV")==0){
		
		char**arguments = (char**)calloc(4, 1);
		for (int i = 0; i < 4; i++)arguments[i] = (char*)calloc(20, 1);
		int n = getTokens(tokens[1], arguments, ",");
		if (arguments[0][0] >= 'A'&&arguments[0][0] <= 'H'&&arguments[0][1] == 'X'&&strlen(arguments[0]) == 2){
			it[index]->opcode = 2;
			it[index]->intstruction_no = index;
			it[index]->op1 = arguments[0][0] - 'A';
				it[index]->op2 = getaddrs(st, arguments[1],sizeofst);
		}
		else{
			it[index]->opcode = 1;
			it[index]->intstruction_no = index;
			it[index]->op1 = getaddrs(st, arguments[0],sizeofst);
			it[index]->op2 =  arguments[1][0] - 'A';
		}
	}
	if (strcmp(tokens[0], "ADD")==0){
		it[index]->opcode = 3;
		it[index]->intstruction_no = index;
		char**arguments = (char**)calloc(4, 1);
		for (int i = 0; i < 4; i++)arguments[i] = (char*)calloc(20, 1);
		int n = getTokens(tokens[1], arguments, ",");
		it[index]->op1 = arguments[0][0] - 'A';
		it[index]->op2 = arguments[1][0] - 'A';
		if (n == 3)it[index]->op3 = arguments[3][0] - 'A';
	}
	if (strcmp(tokens[0], "READ")==0)
	{
		it[index]->opcode = 14;
		it[index]->intstruction_no = index;
		it[index]->op1 = tokens[1][0] - 'A';
	}
	if (strcmp(tokens[0], "PRINT")==0)
	{
		it[index]->opcode = 13;
		it[index]->intstruction_no = index;
		it[index]->op1 = getaddrs(st,tokens[1],sizeofst);
	}
}
void start_program()
{
	char filename[20];
	printf("Enter file name that has to be coverted\n");
	scanf("%s", filename);
	char line[100];
	
	int sizeofsymboltable = getsizeofsymboltable(filename);
	struct symboltable **st = (struct symboltable**)calloc(sizeofsymboltable, sizeof(struct symboltable*));
	for (int r = 0; r < sizeofsymboltable; r++)
		st[r] = (struct symboltable*)calloc(1, sizeof(struct symboltable));
	int symboltableindex = 0;
	
	int intermediatetableindex = 0;
	int sizeofintermediatetable = getsizeofintermediatetable(filename);
	struct intermediate **it = (struct intermediate**)calloc(sizeofintermediatetable, sizeof(struct intermediate*));
	for (int r = 0; r < sizeofintermediatetable; r++)
		it[r] = (struct intermediate*)calloc(1, sizeof(struct intermediate));

	
	FILE *code = fopen(filename, "r");
	while (fgets(line,100,code))
	{
		char **ret = (char**)calloc(1, 30 * sizeof(char*));
		for (int i = 0; i < 30; i++)
			ret[i] = (char*)calloc(1, 20);
		int size = getTokens(line, ret," ");
		
		if (strcmp(ret[0], "DATA") == 0||strcmp(ret[0],"CONST")==0){
			buildsymboltable(ret, st, symboltableindex);
			symboltableindex++;
		}
		else {	
			processtoken(ret, st, sizeofsymboltable ,it, intermediatetableindex);
			intermediatetableindex++;
		
		}
	}
	for (int j = 0; j < sizeofintermediatetable; j++)
		printf("%d  %d   %d  %d  %d   %d\n", it[j]->intstruction_no, it[j]->opcode,it[j]->op1, it[j]->op1,it[j]->op3,it[j]->jump);
	fclose(code);
	return ;
}
int main()
{
	start_program();
	getchar();
	return 0;
}