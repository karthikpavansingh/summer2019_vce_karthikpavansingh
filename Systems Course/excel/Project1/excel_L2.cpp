
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#include<stack>
#define _CRT_SECURE_NO_WARNINGS
using namespace std;
int get(struct cell **data, char c1, char c2);
struct cell
{
	char d[100];
};
int check(char commands[][10], char ch[])
{
	for(int i = 0; i < 5; i++)
		if (strcmp(commands[i],ch) == 0)
			return 1;
	return 0;
}
bool isnumber(char content[])
{
	for (int i = 0; i < strlen(content); i++){
		if (isdigit(content[i]))
			continue;
		else return 0;
	}
	return 1;
}
char *strip_command(char command[])
{
	int l = strlen(command);
	char *ret = (char*)malloc(l);
	int ret_index = 0;
	for (int i = 0; i < l; i++)
	{
		if (command[i] != ' ')
		{
			ret[ret_index++] = command[i];
		}
	}
	ret[ret_index] = '\0';
	return ret;
}
int precedence(char op){
	if (op == '+' || op == '-')
		return 1;
	if (op == '*' || op == '/')
		return 2;
	return 0;
}

// Function to perform arithmetic operations. 
int applyOp(int a, int b, char op){
	switch (op){
	case '+': return a + b;
	case '-': return a - b;
	case '*': return a * b;
	case '/': return a / b;
	}
}
int evaluate(struct cell**data,char content[])
{
	stack <int> values;
	stack <char> ops;
	for (int i = 0; i < strlen(content); i++)
	{

		if (content[i] == '('){
			ops.push(content[i]);
		}
		else if (isdigit(content[i])){
			int num = 0;
			while (i < strlen(content) && isdigit(content[i]))
			{
				
				num = num * 10 + (content[i] - '0');
				i++;
			}
			values.push(num);
			i--;
		}
		else if (content[i] >= 'A'){
			int t = get(data, content[i+1], content[i]);
			values.push(t);
			i = i + 1;
		}
		else if (content[i] == ')')
		{
			while (!ops.empty() && ops.top() != '(')
			{
				int val2 = values.top();
				values.pop();
	
				int val1 = values.top();
				values.pop();

				char op = ops.top();
				ops.pop();

				values.push(applyOp(val1, val2, op));
			}

			ops.pop();
		}
		else
		{
			
			while (!ops.empty() && precedence(ops.top()) >= precedence(content[i])){
				int val2 = values.top();
				values.pop();

				int val1 = values.top();
				values.pop();

				char op = ops.top();
				ops.pop();

				values.push(applyOp(val1, val2, op));
			}


			ops.push(content[i]);
		}
	}
		while (!ops.empty()){
			int val2 = values.top();
			values.pop();

			int val1 = values.top();
			values.pop();

			char op = ops.top();
			ops.pop();

			values.push(applyOp(val1, val2, op));
		}
		return values.top();
}
void set(struct cell **data, char c1,char c2,char content[])
{
	int r = c1 - '1';
	int c = c2 - 'A';
	strcpy_s(data[r][c].d,content);
}
int get(struct cell **data,char c1, char c2)
{
	int r = c1 - '1';
	int c = c2 - 'A';
	if (isnumber(data[r][c].d))
		return atoi(data[r][c].d);
	else evaluate(data,data[r][c].d);	
}
void print(struct cell **data)
{
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			printf("%s ", data[i][j].d);
		}
		printf("\n");
	}
}

void export(char f1[], struct cell **data, char command[])
{
	FILE *f;
	fopen_s(&f, f1, "w");
	for (int i = 0; i < 10; i++){
		for (int j = 0; j < 10; j++)
		{
			if (j != 9)
				fprintf(f, "%d,", get(data,command[4],command[3]));
			else fprintf(f, "%d\n", data[i][j]);
		}
	}
	fclose(f);
}
void import(char f1[], int **data, char command[])
{
	FILE *f;
	char dataToBeRead[100];
	int k = 0; int j = 0;
	fopen_s(&f, f1, "r");
	int v;
	while (fgets(dataToBeRead, 50, f) != NULL)
	{
		for (int i = 0; i < strlen(dataToBeRead); i++){
			if (isdigit(dataToBeRead[i]))
			{
				data[k][j++] = dataToBeRead[i] - '0';
				if (j == 10)
				{
					k++;
					j = 0;
				}
			}
		}
	}
	fclose(f);

}

char *get_content_of_cell(char *s)
{ 
	int l = strlen(s);
	char *ret = (char *)malloc(l);
	int i;
	for ( i = 0; i<l; i++)
	{
		if (s[i] == '=')
			break;
	}
	s[i++] = '\0';
	int index = 0;
	while (i < l)
	{
		ret[index++] = s[i++];
	}
	ret[index] = '\0';
	return ret;
}
int main()
{
	char history[100][100];
	int histindex=0;
	char command[100];
	//char *command = (char*)malloc(100);
	char *exit = "exit";
	char *content;
	printf(">");
	char commands[5][10] = { "GET", "SET", "PRINT", "IMPORT", "EXPORT" };

	char *filename = (char*)malloc(100);

	struct cell **data = (struct cell**)malloc(10 * sizeof(struct cell*));
	for (int i = 0; i < 10; i++){
		data[i] = (struct cell*)malloc(10*sizeof(struct cell));
	}
	while (strcmp(gets_s(command), exit))
	{
		char *s = strip_command(command);
		//if (check(commands, command))
		if (1)
		{
			if (s[0] == 'S'&&s[1] == 'E')
			{
				content=get_content_of_cell(s);
				//printf("%s", content);
				set(data,s[4],s[3],content);
			}
			else if (s[0] == 'G'){
				int a = get(data, s[4], s[3]);
				printf("%d\n", a);
			}
			else if (s[0] == 'P')
				print(data);
			else if (s[0] == 'I'){
				int c = 0;
				int length = strlen(command);
				int position = 8;
				while (c < length) {
					filename[c] = command[position + c - 1];
					c++;
				}

				import(filename, data, command);
			}
			else if (s[0] == 'E')
			{
				int c = 0;
				int length = strlen(command);
				int position = 8;
				while (c < length) {
					filename[c] = command[position + c - 1];
					c++;
				}
				strcpy_s(history[histindex++], filename);
				export(filename, data, command);
				
			}
			else if (s[0] == 'S')
			{
				export(history[histindex-1],data,command);
			}

			
		}
		else{
			printf(">Invalid command\n");
		}

		printf(">");
	}
	return 0;
}
