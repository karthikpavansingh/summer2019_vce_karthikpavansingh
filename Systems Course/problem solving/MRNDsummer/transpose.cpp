#include "stdafx.h"
#include<stdio.h>
#include<stdlib.h>
int **generate_transpose(int **a, int m, int n)
{
	int **r = (int**)malloc(n*sizeof(int*));
	for (int i = 0; i < n; i++)
		r[i] = (int*)malloc(m*sizeof(int));
	for (int i = 0; i < m; i++)
		for (int j = 0; j < n; j++)
			r[j][i] = a[i][j];
	return r;
}
int main()
{
	int m, n;
	printf("Enter no. of rows and columns respectively\n");
	scanf_s("%d%d", &m, &n);
	int **a = (int**)malloc(m*sizeof(int*));
	for (int i = 0; i < m; i++)
		a[i] = (int*)malloc(n*sizeof(int));
	printf("enter i/p array\n");
	for (int i = 0; i < m; i++)
		for (int j = 0; j< n; j++)
			scanf_s("%d", &a[i][j]);
	int **result = generate_transpose(a, m, n);
	printf("Transpose is\n");
	for (int i = 0; i < n; i++)
		for (int j = 0; j< m; j++)
			printf("%d", result[i][j]);

	return 0;
}